export default function errorParse(error:any, custom = false, field = null) {
    if (custom) {
        return {
            status: "error",
            error: { code: 404, message: error, field: field },
        };
    } else if (error.response) {
        let err = error.response.data.error;
        let message = err.message;
        // message = message.split(" ").join("_");
        let code = err.code;
        let field = err.field
        return {
            status: "error",
            error: { code: code, message: message, field: field },
        };
    } else {
        return {
            status: "error",
            error: { code: 404, message: "please check api point or network" },
        };
    }
}
