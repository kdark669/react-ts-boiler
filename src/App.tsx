import React from 'react';
import './App.css';
import MainRoute from "./routes";

const App:React.FC = () => {
  return (
      <MainRoute/>
  );
}

export default App;
