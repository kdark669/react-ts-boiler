import React, {Dispatch, useEffect} from 'react';
import {Route, Redirect, RouteProps} from 'react-router-dom';
import {useDispatch, useSelector} from "react-redux";

import * as actions from '../store/action'
import {ToasterStateInterface} from "../interfaces";
import {RootState} from "../store/interface/storeInterface";

interface PrivateRouteProps extends RouteProps {
    component: React.FC<RouteProps>
    name?: string
}

const PrivateRoute: React.FC<PrivateRouteProps> = (props) => {
    const {component: Component, ...rest} = props;
    const {isLoggedIn} = useSelector((state:RootState) => state.authenticationReducer)

    const openToaster:Function = () => {
        const toasterState: ToasterStateInterface = {
            appear: true,
            title: "error",
            name: "Authentication Error",
            message: "You have to authenticate before proceedings "
        }
        dispatch(actions.setToasterState(toasterState))
    }
    useEffect(() => {
        !isLoggedIn && openToaster()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isLoggedIn])

    const dispatch = useDispatch<Dispatch<any>>()
    return (
        <Route
            {...rest}
            render={props =>
                isLoggedIn ? (
                    <Component {...props} />
                ) : (
                    <Redirect
                        to={'/'}
                    />
                )
            }
        />
    );
};

export default PrivateRoute;
