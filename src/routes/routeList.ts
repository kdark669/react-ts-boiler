import React from 'react'
import Landing from "../modules/landing";
import AdminLayout from "../modules/admin";

let routeList: {
    path: string
    component: React.FC
    name: string
    exact: boolean
    isAuth: boolean
}[];

routeList = [
    {
        path: "/",
        name: "landing",
        exact: true,
        isAuth:false,
        component: Landing
    },
    {
        path: "/admin",
        name: "admin",
        exact: true,
        isAuth:true,
        component: AdminLayout
    }
];

export default  routeList
