export default {
    email: {
        required: true,
        isEmail: true,
        label: 'Email'
    },
    password: {
        required: true,
        minValue: 8,
        label: 'Password'
    }
}

