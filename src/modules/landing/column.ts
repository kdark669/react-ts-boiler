
let listColumn: {
    name: string,
    align: string,
    label: string,
    field: string,
    sliced?: number,
    headerClass:string,
    flexVal: number,
}[];

listColumn = [
    {
        name: "id",
        align: "right",
        label: "ID",
        field: "id",
        headerClass: "entrance-col",
        flexVal: 1,
    },
    {
        name: "first_name",
        align: "left",
        label: "First Name",
        field: "first_name",
        headerClass: "entrance-col",
        flexVal: 6,
    },
    {
        name: "last_name",
        align: "left",
        label: "Last Name",
        field: "last_name",
        headerClass: "entrance-col",
        flexVal: 5,
    },
    {
        name: "email",
        align: "left",
        label: "Email",
        field: "email",
        headerClass: "entrance-col",
        flexVal: 5,
    },
    {
        name: "gender",
        align: "left",
        label: "Gender",
        field: "gender",
        headerClass: "entrance-col",
        flexVal: 4,
    },{
        name: "ip_address",
        align: "left",
        label: "Ip Address",
        field: "ip_address",
        headerClass: "entrance-col",
        flexVal: 4,
    },
];

export default  listColumn
