export type login_user = {
    name?: string
    role?: string
    email: string
    password: string
}

export interface AuthenticationStateInterface {
    login_user: login_user,
    isLoggedIn:boolean
}

export interface AuthenticationPropsInterface {

}
