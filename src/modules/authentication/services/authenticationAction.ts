import * as actionTypes from "./authenticationType"
import {AuthenticationStateInterface, login_user} from "../interface/authenticationInterface";
import * as actions from "../../../store/action"
import {Dispatch} from "react";
import {ToasterStateInterface} from "../../../interfaces";

const loginSuccess:any = (user_detail:AuthenticationStateInterface) => {
    return {
        type: actionTypes.LOGIN_USER,
        login_user: user_detail
    }
}

const logoutSuccess:any = () => {
    return {
        type: actionTypes.LOGOUT_USER,
    }
}

export const checkAuthentication:Function = () => async (dispatch:Dispatch<any>) => {
    let user = localStorage.getItem("user");
    if(!user){
        dispatch(logoutSuccess)
    } else{
        const userDetail = JSON.parse(user);
        const toasterState: ToasterStateInterface = {
            appear: true,
            title: "success",
            name: "User Logged In",
            message: `${userDetail.email} login successfully`
        }
        dispatch(loginSuccess(userDetail))
        dispatch(actions.setToasterState(toasterState))
    }
}

export const loginUser:Function = (userData: login_user) => async (dispatch:Dispatch<any>) => {
    console.log('i am login user')
    console.log(userData)
    const toasterState: ToasterStateInterface = {
        appear: true,
        title: "success",
        name: "User Logged In",
        message: `${userData.email} login successfully`
    }
    actions.setToasterState(toasterState)
}
