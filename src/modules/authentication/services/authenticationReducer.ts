import * as actionTypes from './authenticationType'
import {AuthenticationStateInterface, login_user} from "../interface/authenticationInterface";

const initialState:AuthenticationStateInterface ={
    login_user: {} as login_user ,
    isLoggedIn:false
}

export default (state = initialState, action:any) => {
    const {type,login_user } = action
    switch (type) {
        case actionTypes.LOGIN_USER:
            return {
                ...state,
                login_user: login_user,
                isLoggedIn:true
            }
        case actionTypes.LOGOUT_USER:
            return initialState
        default:
            return state
    }

}

