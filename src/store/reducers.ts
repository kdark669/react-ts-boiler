import { Reducer, combineReducers } from "redux";
import modalReducer from "../common/modal/services/modalReducer";
import loadingReducer from "../common/loading/services/loadingReducer";
import toasterReducer from "../common/toaster/services/toasterReducer";
import authenticationReducer from '../modules/authentication/services/authenticationReducer'

const rootReducer: Reducer = combineReducers({
    modalReducer,
    loadingReducer,
    toasterReducer,
    authenticationReducer
})


export default rootReducer;
