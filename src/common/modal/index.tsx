import React from 'react';
import "./modal.scss";
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../../store/interface/storeInterface";
import * as actions from './services/modalAction'
import {ModalPropsInterface} from "./interface/modalInteraface";

const Modal:React.FC<ModalPropsInterface> = (props) => {
    const { children } = props
    const dispatch = useDispatch()
    const {isOpenModal} = useSelector((state: RootState) => state.modalReducer)
    return (
        <>
            {
                isOpenModal
                && <div className="modal">
                    <div className="modal-backdrop" onClick={() => dispatch(actions.closeModal())} />
                    <div className="modal-card">
                        <div className="modal-close">
                            <i className="material-icons pointer" onClick={() => dispatch(actions.closeModal())} >clear</i>
                        </div>
                        <div className="modal-elements">
                            {
                                children
                            }
                        </div>
                    </div>
                </div>
            }
        </>
    )
};

export default Modal;
