import * as actionType from './loadingType'

export const setLoading:any = ( mode?:string ) => {
    return {
        type: actionType.SET_LOADING,
        mode: mode
    }
}

export const clearLoading:any = () => {
    return {
        type: actionType.CLEAR_LOADING
    }
}
