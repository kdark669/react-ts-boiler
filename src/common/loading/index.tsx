import React from 'react';
import './loading.scss'
import {useSelector} from "react-redux";

import {LoadingPropsInterface} from "./interface/loadingInterface";
import {RootState} from "../../store/interface/storeInterface";

const Loading: React.FC<LoadingPropsInterface> = (props) => {
    const {isLoading} = useSelector((state: RootState) => state.loadingReducer)
    return (
        <>
            {
                isLoading && (
                    <div className="loader-container">
                        <div className="lds-ring">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                )
            }
        </>

    );
};

export default Loading;
