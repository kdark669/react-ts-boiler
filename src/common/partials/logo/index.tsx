import React from 'react';
import './logo.scss'
import LazyImage from '../lazyimg'
interface LogoPropsInterface {

}

const Logo: React.FC<LogoPropsInterface> = () => {
    return (
        <>
            <div className="logo">
                {/*<div className="logo-image">*/}
                {/*   <LazyImage src={'https://cdn.pixabay.com/photo/2020/09/22/22/32/people-5594462__480.jpg'} />*/}
                {/*</div>*/}
                <div className="logo-mini">
                    WoodLand
                </div>
            </div>
        </>
    );
};

export default Logo;
