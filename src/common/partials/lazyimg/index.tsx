import React  from 'react';
import LazyLoad from "react-lazyload";

interface LazyImageProps {
    src:string
    alt?:string
}
const LazyImage:React.FC<LazyImageProps> = (props) => {
    const {
        src,
        alt
    } = props
    return (
        // <LazyLoad >
        //     <img src={src} alt={alt} className={"img"}/>
        // </LazyLoad>
        <img src={src} alt={alt} className={"img"}/>
    );
};

export default LazyImage;
