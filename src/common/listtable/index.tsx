import React, {useEffect, useState} from 'react';
import './listtable.scss'

interface ListTableState {
    columns: any
    rows: any
}

interface ListTableActions {
    deleteAction?: Function
}

type ListTableProps = ListTableState & ListTableActions

const ListTable: React.FC<ListTableProps> = (props) => {
    const {
        columns,
        rows,
        deleteAction,
    } = props

    const sliced = (sliceBy: number, statement: any) => {
        if (sliceBy) {
            return statement.slice(0, sliceBy)
        }
        return statement
    }

    const [paginate, setPaginate] = useState<number>(10)
    const [startingVal, setStartingVal] = useState<number>(0);
    const [endingVal, setEndingVal] = useState<number>(paginate ? paginate : rows.length - 1);
    const [pageNumber, setPageNumber] = useState<number>(1);
    const [totalPages, setTotalPages] = useState<number>(0);

    const paginateRows: Function = (paginationRows: any): void => {
        return paginationRows.slice(startingVal, endingVal);
    }

    useEffect(() => {
        let totalPages: number = parseInt((rows.length / paginate).toFixed(0));
        setTotalPages(totalPages);
    }, [paginate, rows]);

    const paginateData: Function = (val: number): void => {
        if (val === 1) {
            if (endingVal > rows.length - 1) {
                return;
            } else {
                setStartingVal(startingVal + paginate);
                setEndingVal(endingVal + paginate);
            }
        } else {
            if (startingVal < 1) {
                return;
            } else {
                setStartingVal(startingVal - paginate);
                setEndingVal(endingVal - paginate);
            }
        }
        if (pageNumber !== startingVal && pageNumber !== endingVal) {
            setPageNumber(pageNumber + val);
        }
    }

    const changePageTotalData: Function = (e: React.ChangeEvent<HTMLSelectElement>): void => {
        setPaginate(parseInt(e.target.value))
        setEndingVal(parseInt(e.target.value) ? parseInt(e.target.value) : rows.length - 1)
        setPageNumber(1)
        setStartingVal(0)
    }

    return (
        <section className="list-table card d-flex column">
            <div className="table-head d-flex align-center card">
                {
                    columns.map((col: any, key: number) => (
                        <div
                            key={key}
                            className={`table-header ${col.headerClass} flex-${col.flexVal} text-${col.align}`}
                        >
                            {col.label}
                        </div>
                    ))
                }
                <div className="table-header">
                    Actions
                </div>
            </div>
            <div className="table-body card">
                {
                    rows.length !== 0
                        ? <div className={"table-body-listing d-flex column"}>
                            {
                                paginateRows(rows).map((rowData: any, key: number) => (
                                    <div className={"table-row d-flex align-center"} key={key}>
                                        {
                                            columns.map((col: any, key: number) => (
                                                <div
                                                    className={`table-column ${col.headerClass} flex-${col.flexVal} text-${col.align} `}
                                                    key={key}
                                                >
                                                    {
                                                        sliced(col.sliced, rowData[col.field])
                                                    }
                                                </div>
                                            ))
                                        }
                                        <div className="table-actions">
                                            {deleteAction
                                                ? <i
                                                    className="material-icons text-danger"
                                                    onClick={() => deleteAction(rowData.id)}>
                                                    delete
                                                </i>
                                                : ''}
                                        </div>

                                    </div>
                                ))
                            }
                        </div>
                        : <div className="flex-centered">No data found</div>
                }
            </div>
            <div className="table-footer d-flex justify-between align-center items-center">
                <div className="table-name">
                    <div className="form-group">
                        <div className="input-group">
                            <div className="input-box">
                                <select name="page_number"
                                        onChange={(e: React.ChangeEvent<HTMLSelectElement>) => changePageTotalData(e)}>
                                    <option value="10">10</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                {
                    paginate &&
                    <>
                        <div className="form-group">
                            <div className="input-group">
                                <div className="input-box">
                                    <div className="table-button-area d-flex align-center items-center">
                                        <i className="material-icons pointer" onClick={() => paginateData(-1)}>chevron_left</i>
                                        <div className="pageNumber">
                                            <label>{pageNumber} of {totalPages}</label>
                                        </div>
                                        <i className="material-icons pointer" onClick={() => paginateData(1)}>chevron_right</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </>
                }
            </div>
        </section>
    );
};

export default ListTable;
